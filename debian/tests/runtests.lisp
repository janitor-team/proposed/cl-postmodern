(require "asdf")

;; CL-IRONCLAD does not compile against recent SBCL on 32-bit ARM.
;; See Debian bug#1016750.
(when (and (member :sbcl *features*)
	   (member (or (uiop:getenv "DEB_HOST_ARCH")
		       (uiop:stripln
			(uiop:run-program '("dpkg" "--print-architecture")
					  :output :string)))
		   '("armel" "armhf") :test #'string=))
  (uiop:quit 0))

(let ((asdf:*user-cache* (uiop:getenv "AUTOPKGTEST_TMP"))) ; Store FASL in some temporary dir
  #+(or ecl clisp) ; On those, it it necessary to manually load usocket
  (asdf:load-system "usocket")

  (asdf:load-system "cl-postgres")
  (asdf:load-system "s-sql")
  (asdf:load-system "postmodern")
  (asdf:load-system "simple-date")
  (asdf:load-system "simple-date/tests"))

;; Can't use ASDF:TEST-SYSTEM, its return value is meaningless
(let ((results (5am:run :simple-date)))
  (5am:explain! results)
  (unless (5am:results-status results)
    (uiop:quit 1)))

;; Can't run tests for other systems, they require a postgresql server running
